#!/bin/bash

DEPLOY_HOST="raif-prod.google.com"
PROJECT_DIR=/home/projects/datafest

scp docker-compose.yml Dockerfile requirements.txt datafest.sql .env ${DEPLOY_HOST}:${PROJECT_DIR}
ssh $DEPLOY_HOST "mkdir -m 775 ${PROJECT_DIR}/applications"
ssh $DEPLOY_HOST "mkdir -m 775 ${PROJECT_DIR}/airflow"
ssh $DEPLOY_HOST "mkdir -m 775 ${PROJECT_DIR}/airflow/logs; mkdir -m 775 ${PROJECT_DIR}/airflow/plugins"

ssh $DEPLOY_HOST "cd ${PROJECT_DIR} && sudo docker-compose down; sudo docker-compose up airflow-init"
ssh $DEPLOY_HOST "cd ${PROJECT_DIR} && sudo docker-compose up -d"

import os
import pickle
import logging
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from utils.logger_utils import influx_logger_init
from utils.influx_utils import InfluxConnector
from utils.date_utils import one_month_back, one_year_back
from utils.postgres_utils import PostgresConnector


def fit(**kwargs):

    start_date = kwargs['execution_date'].strftime('%Y-%m')
    config = kwargs['params']['config']

    adapter_dict = {
        "stage_name": "Training",
        "dag_date": start_date,
    }
    logger = influx_logger_init(adapter_feed_dict=adapter_dict,
                                config=config,
                                influx_measurement="logs",)

    try:

        # Get data
        training_end_date = one_month_back(start_date)
        training_start_date = one_year_back(training_end_date)
        X_train, y_train = PostgresConnector(config=config).get_train(training_start_date, training_end_date)

        # Train
        logger.info(f'Training for {start_date} started')
        scaler = StandardScaler()
        lr = LinearRegression()
        X_train = scaler.fit_transform(X_train)
        lr.fit(X_train, y_train)
        logger.info(f'Training for {start_date} finished')

        # Save fitted model and preprocessor
        current_dir = os.path.dirname(os.path.abspath(__file__))
        parent_dir = os.path.dirname(current_dir)

        model_path = os.path.join(parent_dir, 'data', 'models', f'{start_date}.pkl')
        with open(model_path, 'wb') as f:
            pickle.dump(lr, f)
        logger.info(f'Model saved to {model_path}.')

        preprocessor_path = os.path.join(parent_dir, 'data', 'preprocessors', f'{start_date}.pkl')
        with open(preprocessor_path, 'wb') as f:
            pickle.dump(scaler, f)
        logger.info(f'Preprocessor saved to {preprocessor_path}.')

    except Exception as e:
        logger.error(e, exc_info=True)
        raise e


def predict(**kwargs):

    start_date = kwargs['execution_date'].strftime('%Y-%m')
    config = kwargs['params']['config']
    branch_name = config['branch_name']

    adapter_dict = {
        "stage_name": "Prediction",
        "dag_date": start_date,
    }
    logger = influx_logger_init(adapter_feed_dict=adapter_dict,
                                config=config,
                                influx_measurement="logs")
    try:
        logger.info(f'Prediction for {start_date} stared')

        # Load features
        df = PostgresConnector(config=config).get_features(start_date=start_date,
                                              end_date=start_date)

        # Load models and preprocessors
        current_dir = os.path.dirname(os.path.abspath(__file__))
        parent_dir = os.path.dirname(current_dir)

        model_path = os.path.join(parent_dir, 'data', 'models', f'{start_date}.pkl')
        with open(model_path, 'rb') as f:
            lr = pickle.load(f)
        logger.info(f'Model {model_path} loaded')

        preprocessor_path = os.path.join(parent_dir, 'data', 'preprocessors', f'{start_date}.pkl')
        with open(preprocessor_path, 'rb') as f:
            preprocessor = pickle.load(f)
        logger.info(f'Preprocessor {preprocessor_path} loaded')

        # Get predicted value
        predict = lr.predict(preprocessor.transform(df[['wind', 'solar']]))[0]
        print("Predict $$$", predict, type(predict))
        logger.info(f'Predicted consumption for date {start_date} = {predict}')

        # Write predict to postgres
        PostgresConnector(config=config).write_predicts(start_date, branch_name, predict)

    except Exception as e:
        logger.error(e, exc_info=True)
        raise e


def statistics(**kwargs):

    start_date = kwargs['execution_date'].strftime('%Y-%m')
    config = kwargs['params']['config']
    branch_name = config['branch_name']

    adapter_dict = {
        "stage_name": "Statistics",
        "dag_date": start_date,
    }
    logger = influx_logger_init(adapter_feed_dict=adapter_dict,
                                config=config,
                                influx_measurement="logs")

    try:
        target_date = one_month_back(start_date)
        postgres = PostgresConnector(config=config)
        influx = InfluxConnector(config=config)

        logger.info(f"Statistics collection started for {target_date}")

        target = postgres.get_target(start_date=target_date, end_date=target_date)
        predict = postgres.get_predicts(start_date=target_date, end_date=target_date, branch_name=branch_name)

        if len(predict) > 0:

            target_value = target['consumption'].values[0]
            influx.write_target_point(target_value=target_value, ts=target_date, branch_name=branch_name)

            predict_value = predict['predict'].values[0]
            influx.write_score_point(score_value=predict_value, ts=target_date, branch_name=branch_name)

            metric_value = abs(target_value - predict_value)
            influx.write_metric_point(metrics_value=metric_value, ts=target_date, branch_name=branch_name)

            logger.info(f'Metrics is {metric_value}')
        else:
            logger.error(f"No predict for {target_date} found")

    except Exception as e:
        logger.error(e, exc_info=True)
        raise e

import logging
from utils.influx_utils import InfluxHandler


def influx_logger_init(adapter_feed_dict, config: dict, log_level=logging.INFO, influx_measurement="logs"):
    logger = logging.getLogger(__name__)
    handler = InfluxHandler(measurement=influx_measurement, config=config)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('''%(asctime)s - %(levelname)s - %(dag_date)s - %(stage_name)s - %(message)s''')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(log_level)
    logger = logging.LoggerAdapter(logger, adapter_feed_dict)
    return logger
